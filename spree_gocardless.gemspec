

# # encoding: UTF-8
Gem::Specification.new do |s|
  s.platform    = Gem::Platform::RUBY
  s.name        = 'spree_gocardless'
  s.version     = '1'
  s.summary     = 'gocardless payment integration'
  s.description = 'gocardless payment integration'
  s.required_ruby_version = '>= 1.8.7'

  s.author    = 'FarmDrop'
  s.email     = 'tech@farmdrop.co.uk'
  s.homepage  = 'http://www.farmdrop.co.uk'

  s.require_path = 'lib'
  s.requirements << 'none'
  
  s.add_dependency 'spree_core', '~> 1.3.2'

  # s.add_development_dependency 'capybara', '~> 1.1.2'
  # s.add_development_dependency 'coffee-rails'
  # s.add_development_dependency 'factory_girl', '~> 2.6.4'
  # s.add_development_dependency 'ffaker'
  # s.add_development_dependency 'rspec-rails',  '~> 2.9'
  # s.add_development_dependency 'sass-rails'
  # s.add_development_dependency 'sqlite3'
end
